import { NgModule }              from '@angular/core';
import { RouterModule, Routes }  from '@angular/router';
import { RequestListComponent }  from './requestList/requestList.component';
import { RequestInfoComponent }  from './requestInfo/requestInfo.component';
import { PageNotFoundComponent } from './not-found.component';

const appRoutes: Routes = [
    { path: 'request-list', component: RequestListComponent },
    { path: 'request-info/:id', component: RequestInfoComponent },
    { path: '',   redirectTo: '/request-list', pathMatch: 'full' },
    { path: '*', component: PageNotFoundComponent }
];

@NgModule({
    imports: [
        RouterModule.forRoot(appRoutes)
    ],
    exports: [
        RouterModule
    ]
})
export class AppRoutingModule {}