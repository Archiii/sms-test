import { Component, OnInit}               from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';

import 'rxjs/add/operator/switchMap';

import { Request }        from '../entity/request'
import { RequestService } from '../services/request.service';
import {SharedService}    from "../services/shared.service";

@Component({
	selector: 'request-info',
	templateUrl: 'app/requestInfo/requestInfo.component.html',
	styleUrls: ['app/requestInfo/requestInfo.component.less'],
    providers: [RequestService, SharedService]
})
export class RequestInfoComponent  implements OnInit{

    request: Request;

    constructor(
        private route: ActivatedRoute,
        private router: Router,
        private requestService: RequestService,
        private sharedService: SharedService
    ){}

    ngOnInit() {
        this.getRequest();
    }

    getRequest() {
        this.route.params
            .switchMap((params: Params) => this.requestService.getRequest(+params['id']))
            .subscribe((request: Request) => this.request = request);
        this.sharedService.emitChange("Заявка " + this.request.number + " " + this.request.equipment);
    }

    gotoList() {
        this.router.navigate(['/request-list']);
    }
}
