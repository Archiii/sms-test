// enum State { 'Не рассмотренная', 'Разрешенная', 'Открытая' }
// enum Form { 'ТР', 'КР', 'СР' }
// enum Category { 'ПЛ', 'НПЛ' }

export class Request{
    public id: number;
    public number: string;
    public organization: string;
    public state: string;
    public object: string;
    public equipment: string;
    public cumulativeStart: Date;
    public cumulativeEnd: Date;
    public allowedStart: Date;
    public allowedEnd: Date;
    public form: string;
    public category: string;
    public upr: string;

    // constructor(number: string, organization: string, state: string, object: string, equipment: string, cumulativeStart: Date, cumulativeEnd: Date,
    //             allowedStart: Date, allowedEnd: Date, form: string, category: string, upr: string) {
    //     this.id = Request.id++;
    //     this.number = number;
    //     this.organization = organization;
    //     this.state = state;
    //     //this.state = State[state];
    //     this.object = object;
    //     this.equipment = equipment;
    //     this.cumulativeStart = cumulativeStart;
    //     this.cumulativeEnd = cumulativeEnd;
    //     this.allowedStart = allowedStart;
    //     this.allowedEnd = allowedEnd;
    //     this.form = form;
    //     this.category = category;
    //     //this.form = Form[form];
    //     //this.category = Category[category];
    //     this.upr = upr;
    // }

    //  constructor(json: any){
    //      this.id = 0;
    //      this.number = json.number;
    //      this.organization = json.organization;
    //      this.state = json.state;
    //      this.object = json.object;
    //      this.equipment = json.equipment;
    //      this.cumulativeStart = json.cumulativeStart;
    //      this.cumulativeEnd = json.cumulativeEnd;
    //      this.allowedStart = json.allowedStart;
    //      this.allowedEnd = json.allowedEnd;
    //      this.form = json.form;
    //      this.category = json.category;
    //      this.upr = json.upr;
    //  }
    //
    // static id: number = 0;
}