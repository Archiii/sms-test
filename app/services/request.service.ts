import {Injectable}     from '@angular/core';
import {Http, Response} from '@angular/http';
import {Observable}     from "rxjs";

import {Request} from '../entity/request';

@Injectable()
export class RequestService{
    private URL = "./app/data/requests.json";

    constructor(private http:Http) {}

    getRequests(): Observable<Request[]> {
        return this.http.get(this.URL)
            .map((response:Response) => response.json()) // , ((request) => new Request(request))
            .catch((error:any) => Observable.throw(error.json().error || 'Server error'));
    }

    getRequest(id: number): Observable<Request> {
        return this.http.get(this.URL)
            .map((response:Response) => response.json())
            .filter((request:Request) => request.id == id)
            .catch((error:any) => Observable.throw(error.json().error || 'Server error'));
    }
}