import { Component }     from '@angular/core';
import { SharedService } from "./services/shared.service";

@Component({
    selector: 'my-app',
    templateUrl: 'app/app.component.html',
    styleUrls: ['app/app.component.less'],
    providers: [SharedService]
})
export class AppComponent {
    name: string = "Список заявок";
    user: string = "Иванов Иван Иванович";

    constructor(
        private sharedService: SharedService
    ) {
        sharedService.changeEmitted$.subscribe(
            text => {
                this.name = text;
            });
    }
}