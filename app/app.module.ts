import { NgModule }      from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule }   from '@angular/forms';
import { CommonModule }  from '@angular/common';
import { HttpModule }    from '@angular/http';

import { AppComponent }  from './app.component';
import { AppRoutingModule } from './app-routing.module';

import { RequestListComponent }  from './requestList/requestList.component';
import { RequestInfoComponent }  from './requestInfo/requestInfo.component';
import { PageNotFoundComponent } from './not-found.component';

@NgModule({
    imports: [
        BrowserModule,
        HttpModule,
        FormsModule,
        CommonModule,
        AppRoutingModule
    ],
    declarations: [
        AppComponent,
        RequestListComponent,
        RequestInfoComponent,
        PageNotFoundComponent
    ],
    bootstrap: [AppComponent]
})

export class AppModule {}
