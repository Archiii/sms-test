import { Component, OnInit} from '@angular/core';
import { Router }           from '@angular/router';

import { Request }        from '../entity/request'
import { RequestService } from '../services/request.service';
import { SharedService }  from "../services/shared.service";

@Component({
	selector: 'request-list',
	templateUrl: 'app/requestList/requestList.component.html',
	styleUrls: ['app/requestList/requestList.component.less'],
    providers: [RequestService, SharedService]
})
export class RequestListComponent implements OnInit {
    name: string = "Список заявок";
    requests: Request[];
    pageCount: number = 2;
    recordCount: number = 0;
    recordsOnPage: number[] = [50, 100, 200];
    currentPage: number = 1;

    constructor(
        private router: Router,
        private requestService: RequestService,
        private sharedService: SharedService
    ){}

    ngOnInit(): void {
        this.sharedService.emitChange("Список заявок");
        this.getRequests();
    }

    getRequests(): void {
        this.requestService.getRequests()
            .subscribe(
                requests => {
                    this.requests = requests;
                    this.recordCount = this.requests.length;
                },
                err => {
                    console.log(err);
                })
    }

    onSelect(request: Request) {
        this.router.navigate(['/request-info', request.id]);
    }
}
